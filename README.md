```html
"require": {
        "wrklst/docxmustache": "^0.0.23",
        "intervention/image": "^2.4.1"
    },
```


open the config/app.php file. 
Add this to the $providers array.
```php
Intervention\Image\ImageServiceProvider::class
```


Next add this to the $aliases array.
```php
'Image' => Intervention\Image\Facades\Image::class
```


add route
```php
Route::resource('report', 'ReportController');
```


open the config/filesystems.php file.   
add config to custom disk 
```php
'report' => [
            'driver' => 'local',
            'root' => storage_path(),
        ],
```


copy paste template ke 

```html
./storage/app/
```
##CONTOH

Tambahkan `[IMG-REPLACE] link image [/IMG-REPLACE]` pada setiaap link image
contoh:     
`
[IMG-REPLACE]http://www.germandrive.com/wp-content/uploads/2017/05/placeholder.gif[/IMG-REPLACE]
`

untuk list data yang sudah diparsing bisa dilihat di 
```html
/report
```
Link untuk generate report showroom
```html
/showroom
```

Link untuk generate report handover
```html
/handover
```

Contoh pengunaan:

```html
{{grade_score}}

```
untuk mendapetkan data grade score  dll, untuk tag2 datanya bisa dilihat di /report

Contoh jika datanya array 2 dimensi bisa mengunakan titik
```html

{{appraise_info.customer_email}}
```

pada template di image appraise photo itu saya loop tp kayaknya hasil view nya kurang bagus.
jadi mungkin bisa di parsing 1 1 di templatenya.

## Replacing images

The image needs to be a reachable URL with a image in a supported format. The url value needs to be placed into the alt text description field of the image.
Images will be resampled to the constraints of the placeholder image.
The Image value needs to be formated the with pseudo tags around, such as:
`[IMG-REPLACE]http://placehold.it/350x150[/IMG-REPLACE]`

## Example
Please also checkout the example in the example folder to get a basic understand of how to use this class.

## DOCX to PDF conversion

Conversion to PDF requires `libreoffice-common` to be installed on the server (used for conversion).
Use `sudo apt install libreoffice-common` on your ubuntu/debian based server. Also install ttf-mscorefonts if you need support for Arial font when converting docx documents to pdf `sudo apt-get install ttf-mscorefonts-installer `

untuk lebih jelasnya bisa lihat [docs](https://github.com/wrklst/docxmustache)

